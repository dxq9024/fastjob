bui.ready(function(){
    // 列表页脚本
    var sortDropdown = bui.dropdown({
        id: ".sbui-dropdown",
        relative: true,
        handle: '.sbui-target'
    });

    var listFilter = bui.select({
        title: '筛选',
        id: "#list-filter-dialog",
        trigger: "#list-filter",
        effect: "fadeInRight",
        position: "right",
        fullscreen: true,
        mask: true,
    });
    $('#list-filter-dialog-ok').on('click', function (e) {
        listFilter.hide();
    });
    $('#list-filter-dialog-cancel').on('click', function (e) {
        listFilter.hide();
    });

    // 详情页脚本
    var uiActionsheet = bui.actionsheet({
        trigger: "#job-detail-share-btn",
        buttons: [{ name: "分享到微博", value: "weibo" }, { name: "分享到微信", value: "weixin" }, { name: "分享到QQ", value: "QQ" }],
        callback: function(e) {
            var val = $(e.target).attr("value");

            if (val == "cancel") {
                this.hide();
            }
        }
    });

    var notloginDialog = bui.dialog({
        id: "#job-detail-apply-notlogin-dialog",
        height: 150,
        width: 300,
        mask: true,
        callback: function(e) {
            console.log(e.target)
        }
    });
    var loginDialog = bui.dialog({
        id: "#job-detail-apply-login-dialog",
        height: 150,
        width: 300,
        mask: true,
        callback: function(e) {
            console.log(e.target)
        }
    });
    $('#job-detail-apply-btn').on("click",function  () {
        notloginDialog.open();
    });

    // 注册页面
    $('#job-register-tab .bui-nav .bui-btn').on('click', function(e) {
        var index = $(e.target).index();
        $(e.target).siblings().attr('class', 'bui-btn');
        $(e.target).attr('class', 'bui-btn active');
        $('.bui-tab-main-item').css('display', 'none');
        $('.bui-tab-main-item').eq(index).css('display', 'block');
    });
    // 我要招聘
    bui.select({
        id:"#job-hiring-post-select-dialog",
        trigger:"#job-hiring-post-select",
        buttons: []
    });
    bui.select({
        id:"#job-hiring-city-select-dialog",
        trigger:"#job-hiring-city-select",
        buttons: []
    });
    bui.select({
        id:"#job-hiring-salary-select-dialog",
        trigger:"#job-hiring-salary-select",
        buttons: []
    });
});
